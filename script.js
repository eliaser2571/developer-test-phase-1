var $ = jQuery.noConflict();
$(document).ready(function () {
    $('.body > button').click(function(e) {
        e.preventDefault();
        $('.body > button').removeClass('selected');
        $(this).addClass('selected');
        var label = $(this).text();
        $('.footer p span').text(label);
    });
});
