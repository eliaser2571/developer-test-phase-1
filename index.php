<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Island Media Management - Developer Test Phase 1</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="main.css">
            </head>
    <body>
        <?php
            $days = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
            $today = date("l");
        ?>
        <div id="container">
            <div class="header">
                <p>Today Is<span><?php  if (in_array($today, $days)) { echo $today; } ?></span></p>
            </div>
            <div class="body">
                <?php
                    foreach ($days as $value) { 
                        echo '<button class="'.($today == $value ? 'active' : '').'">'.$value.'</button>';
                    }
                ?>
            </div>
            <div class="footer">
                <p>Selected day is<span>...</span></p>
            </div>
        </div>
        <script src="http://code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="script.js"></script>
    </body>
</html>
